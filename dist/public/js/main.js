(function($){
    $.fn.disableSelection = function() {
        return this
            .attr('unselectable', 'on')
            .css('user-select', 'none')
            .on('selectstart', false);
    };
})(jQuery);

Number.prototype.bound = function(low/*Number*/, high/*Number*/){
    if(!(isNaN(low) || isNaN(high)) && low > high){
        throw new Error("low cannot be greater than high");
    }
    if(high !== undefined && this > high){ 
        return high;
    }
    if(low !== undefined && this < low){
        return low;
    }
    return this.valueOf()/*Number*/;
};
